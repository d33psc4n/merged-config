import config from 'config'
import fs from 'fs'
import { fileURLToPath } from 'url'
import path, { dirname } from 'path'

const stripOfSpecialChars = (key) => key.replace(/[*?]/g, '')

const getConfigOverride = (meta) => {
  const __filename = fileURLToPath(meta.url)
  const __dirname = dirname(__filename)
  const pathJoined = path.join(__dirname, './config.json')
  return fs.existsSync(pathJoined) ? JSON.parse(fs.readFileSync(pathJoined, 'utf-8')) : {}
}

const getKey = (key) => {
  const strippedKey = stripOfSpecialChars(key)
  if (key.includes('?')) {
    if (config.has(strippedKey)) {
      return config.get(strippedKey)
    }
  }
  return config.get(strippedKey)
}

const getObject = (key) => {
  const obj = getKey(key)
  if (typeof obj !== 'object') throw new Error(`${key} is not an object, cannot use *`)
  return obj
}

const getLastName = (key) => {
  const splitProps = key.split('.')
  return stripOfSpecialChars(splitProps[splitProps.length - 1])
}

/**
 * Get merged config from node config and local config.json in directory
 * If arg ends with *, it destructures everything, otherwise sets the last from '.' split as the object key
 * If arg ends with ? it's optional
 * @param  {String} importMeta import.meta
 * @param  {} ...args Args for the config object.
 */
export default (importMeta, ...args) => {
  let conf = {}
  args.forEach((arg) => {
    if (arg.includes('*')) {
      conf = { ...conf, ...getObject(arg) }
    } else {
      conf[getLastName(arg)] = getKey(arg)
    }
  })
  return { ...conf, ...getConfigOverride(importMeta) }
}
